import React, { Component } from "react";
import "../style/styleButtons.scss";
import "../style/styleModal.scss";
import Button from "./Button";
import Modal from "./Modal";
class App extends Component {
  state = {
    isOpenFirstModal: false,
    isOpenSecondModal: false,
  };
  render() {
    const modalOne = {
      header: " Do you want to delete this file?",
      text: [
        <p>
          Once you delete this file? it won`t be possible to undo this action.
        </p>,
        <p> Are you sure you want to delete it?</p>,
      ],
      actions: [
        <button className="modal-buttton" onClick={this.handleClickFirstModal}>
          Yes
        </button>,
        <button className="modal-buttton" onClick={this.handleClickFirstModal}>
          No
        </button>,
      ],
    };
    const modalTwo = {
      header: "Delete your Account?",
      text: [
        <p>This action is final and you will be unable to recover any data</p>,
        <p>Are you confirming the action?</p>,
      ],
      actions: [
        <button className="modal-buttton" onClick={this.handleClickSecondModal}>
          Yes
        </button>,
        <button className="modal-buttton" onClick={this.handleClickSecondModal}>
          No
        </button>,
      ],
    };
    const firstModal = this.state.isOpenFirstModal && (
      <Modal
        header={modalOne.header}
        text={modalOne.text}
        actions={modalOne.actions}
      />
    );
    const secondModal = this.state.isOpenSecondModal && (
      <Modal
        header={modalTwo.header}
        text={modalTwo.text}
        actions={modalTwo.actions}
      />
    );
    return (
      <div>
        <Button
          backgroundColor="double-border-button"
          text="Open first modal"
          onClick={this.handleClickFirstModal}
        />
        <Button
          backgroundColor="double-border-button double-border-button-red"
          text="Open second modal"
          onClick={this.handleClickSecondModal}
        />
        {firstModal}
        {secondModal}
      </div>
    );
  }
  handleClickFirstModal = () => {
    this.setState({
      isOpenFirstModal: !this.state.isOpenFirstModal,
      isOpenSecondModal: false,
    });
  };

  handleClickSecondModal = () => {
    this.setState({
      isOpenSecondModal: !this.state.isOpenSecondModal,
      isOpenFirstModal: false,
    });
  };
}

export default App;
