import React, { Component } from "react";

class Modal extends Component {
  state = {
    isOpenModal: true,
  };
  render() {
    const modal = this.state.isOpenModal && (
      <div className={"modal modal-content"}>
        <div className={"modal__content-header"}>
          <h3 className="modal-header">{this.props.header}</h3>
          <button className="modal-buttton" onClick={this.clickCloseModal}>
            X
          </button>
        </div>
        <p className="modal__content-main">{this.props.text}</p>
        <div className="modal-btn-flex">
          <div>{this.props.actions}</div>
        </div>
      </div>
    );
    return <div>{modal}</div>;
  }
  clickCloseModal = () => {
    this.setState({
      isOpenModal: !this.state.isOpenModal,
    });
  };
}

export default Modal;
